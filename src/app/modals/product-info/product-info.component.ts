import { Component, OnInit } from '@angular/core';

import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { DatasimService } from 'src/app/services/datasim/datasim.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styles: []
})
export class ProductInfoComponent implements OnInit {
  showItemAddedMsg: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private datasim: DatasimService) {
    
  }

  ngOnInit(): void {
  }

  getProductRating(sifraProizvoda) {
    let rating = 0;

    this.datasim.ordersHistory.forEach(item => {
      let products = item.listaProizvoda;

      products.forEach(product => {
        if (product.rated === true && product.rate > 0 && product.sifraProizvoda == sifraProizvoda) {
          let productRate = Number.parseInt(product.rate);

          rating += productRate;
        }
      });
    });

    if (rating) {
      return rating.toFixed(2);
    }

    return "Nije ocenjeno";
  }

  addToCart(product) {
    let data = {
      "product": product.name,
      "vendor" : product.vendor,
      "quantity" : 1,
      "price" : product.price,
      "image" : product.image,
      "sifraProizvoda": product.productCode,
    };

    this.showItemAddedMsg = true;

    this.datasim.addToCart(data);

    setTimeout(() => {this.showItemAddedMsg = false;}, 3000);
  }
}

import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatasimService } from 'src/app/services/datasim/datasim.service';
import { MatTable } from '@angular/material/table';
import { LoginService } from 'src/app/services/auth/login/login.service';
import { OrdersComponent } from 'src/app/components/orders/orders.component';

@Component({
  selector: 'app-orders-modal',
  templateUrl: './orders-modal.component.html',
  styles: []
})
export class OrdersModalComponent implements OnInit {
  currentRating: string = "5";
  commentBoxShow: boolean;
  comment: string;
  commentBox:any[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private datasim: DatasimService, private loginService: LoginService) { }


  ngOnInit(): void {
  }


  rateVendor(sifraPorudzbine, sifraProizvoda) {
    let textareaBox = (<HTMLInputElement>document.getElementById(`commentBox${sifraProizvoda}`));
    let comment = textareaBox != null ? textareaBox.value : null;

    let data = {
      rated: true,
      rate: this.currentRating,
      comment: comment,
      user: this.loginService.getUserData().username,
    }

    this.datasim.updateOrderData(sifraPorudzbine, sifraProizvoda, data);
  }

  addCommentBox(sifraProizvoda) {
    let textareaBox = (<HTMLInputElement>document.getElementById(`commentBoxDiv${sifraProizvoda}`));
    let hidden = textareaBox.style.display == "none";
    let hideshow = "none";

    if (hidden) {
      hideshow = "block";
    }

    textareaBox.style.display = hideshow;
  }
}

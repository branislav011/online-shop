import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  users:Array<User> = [
    {
      userId: 1,
      username: "Bane",
      password: "123",
      firstname: "Branislav",
      lastname: "Markovic",
      email: "cikabane@gmail.com",
      phone: "+381645555",
      address: "Cika Banetova 8",
      products: ["Audio komponente"],
    },
    {
      userId: 2,
      username: "perica@gmail.com",
      password: "123456",
      firstname: "Pera",
      lastname: "Peric",
      email: "pera_s_onoga_sveta@gmail.com",
      phone: "+381645555",
      address: "Perina 8",
      products: ["Zvucnici"],
    },
  ];

  logged: boolean;
  userData: any;

  constructor() { }

  validateLoginParams(username: string, pass: string): boolean {
    let userExists = this.userExists(username, pass);
    username = username.trim();
    pass = pass.trim();

    if (!username || !pass || !userExists) {
      return false;
    }

    return true;
  }

  userExists(username: string, pass: string): boolean {
    let userExists = this.users.find(user => user.username == username && user.password == pass);

    return !userExists ? false : true;
  }

  login(username: string, pass: string): boolean {
    let valid = this.validateLoginParams(username, pass);

    if (!valid) {
      return false;
    }

    let usersData = this.users.find(user => user.username == username);
   
    //save user data
    this.logged = true;
    this.userData = usersData;

    return true;
  }

  getUsers() {
    return this.users;
  }

  getUserData() {
    return this.userData;
  }

  setUserData(data: any) {
    this.userData = data;
  }

  getLastUserId() {
    let userId = null;
    let usersCount = this.users.length;

    this.users.forEach((user, index) => {
      if ((index + 1) == usersCount) {
        userId = user.userId;
      }
    });

    return ++userId;
  }
}

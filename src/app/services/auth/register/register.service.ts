import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user/user';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  constructor(private loginService: LoginService) {

  }

  validateLoginParams(username: string, password: string, repeatPassword: string): boolean {
    let userExists = this.userExists(username);
    username = username.trim();
    password = password.trim();
    repeatPassword = repeatPassword.trim();

    if (!username || !password || (password != repeatPassword) || userExists) {
      return false;
    }

    return true;
  }

  userExists(username: string): boolean {
    let userExists = this.loginService.getUsers().find(user => user.username == username);

    return !userExists ? false : true;
  }

  register(newUserData) {
    let username = newUserData.username;
    let password = newUserData.password;
    let repeatPassword = newUserData.repeatPassword;

    let valid = this.validateLoginParams(username, password, repeatPassword);

    if (!valid) {
      return false;
    }

    this.loginService.users.push(newUserData);

    return true;
  }
}

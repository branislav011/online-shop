import { Injectable } from '@angular/core';
import { Orders } from 'src/app/interfaces/orders/orders';
import { LoginService } from '../auth/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class DatasimService {
  public ordersActive: Array<Orders> = [
    {
      "userId" : 1,
      "sifra": 6, 
      "datum" : "13.04.2021 17:46", 
      "status" : "Tekuca", 
      "proizvodi" : "description",
      "vendorImg" : "av_market.png",
      "listaProizvoda" : [
        {"sifraProizvoda": "1", "vendor" : "AV Market doo", "product" : "Yamaha Receiver R-S700", "rated" : false, "rate" : null, comment: null},
      ]
    },
  ];

  ordersHistory: Array<Orders> = [
    {
      "userId" : 1,
      "sifra": 1,
      "datum" : "22.05.2021 04:05", 
      "status" : "Zavrsena", 
      "proizvodi" : "description",
      "vendorImg" : "tehnomanija.jpg",
      "listaProizvoda" : [
        {"sifraProizvoda": "2","vendor" : "Tehnomanija", "product" : "Yamaha Receiver RX-V373", "rated" : false, "rate" : null, "quantity" : 1,  comment: null},
        {"sifraProizvoda": "3","vendor" : "Tehnomanija", "product" : "Marantz SR5013 Receiver", "rated" : false, "rate" : null, "quantity" : 1,  comment: null},
        {"sifraProizvoda": "16","vendor" : "Gigatron", "product" : "GIGATRON AURORA LIDER Racunar", "rated" : false, "rate" : null, "quantity" : 1,  comment: null},
        {"sifraProizvoda": "17","vendor" : "Win Win", "product" : "Desk Home Racunar", "rated" : false, "rate" : null, "quantity" : 1,  comment: null},
      ]
    },
    {
      "userId" : 1,
      "sifra": 2, 
      "datum" : "07.06.2021 12:12", 
      "status" : "Zavrsena", 
      "proizvodi" : "description",
      "vendorImg" : "av_market.png",
      "listaProizvoda" : [
        {"sifraProizvoda": "1", "vendor" : "AV Market doo", "product" : "Yamaha Receiver R-S700", "rated" : false, "rate" : null, "quantity" : 1,  comment: null}
      ]
    },
    {
      "userId" : 1,
      "sifra": 3, 
      "datum" : "11.10.2021 07:08", 
      "status" : "Zavrsena", 
      "proizvodi" : "description",
      "vendorImg" : "av_market.png",
      "listaProizvoda" : [
        {"sifraProizvoda": "3", "vendor" : "Tehnomanija", "product" : "Marantz SR5013 Receiver", "rated" : false, "rate" : null, "quantity" : 1,  comment: null}
      ]
    },
    {
      "userId" : 2,
      "sifra": 4, 
      "datum" : "02.04.2021 19:37", 
      "status" : "Zavrsena", 
      "proizvodi" : "description",
      "vendorImg" : "av_market.png",
      "listaProizvoda" : [
        {"sifraProizvoda": "4", "vendor" : "Tehnomanija", "product" : "Marantz Pojacalo PM8006", "rated" : true, "rate" : 4,  comment: {
          "comment" : "Odlican proizvod",
          "user" : "Bane",
          "date" : "03.04.2021 17:22",
        }}
      ]
    },
    {
      "userId" : 1,
      "sifra": 5, 
      "datum" : "12.04.2021 12:02", 
      "status" : "Otkazana", 
      "proizvodi" : "description",
      "listaProizvoda" : [
        {"sifraProizvoda": "1", "vendor" : "4Audio", "product" : "Yamaha receiver", "rated" : false, "rate" : null,  comment: null}
      ]
    },
  ];

  cartItems: Array<any> = [];
  total: number;

  constructor(private loginService: LoginService) { }

  getOrderDataActive() {
    let userId = this.loginService.getUserData().userId;
    const orders = this.ordersActive.filter(order => order.userId == userId);

    return orders;
  }

  getOrderDataHistory() { //zavrsene i otkazane
    let userId = this.loginService.getUserData().userId;
    const orders = this.ordersHistory.filter(order => order.userId == userId);
    
    return orders;
  }

  getOrderDataHistoryAll() {
    const orders = this.ordersHistory;
    
    return orders;
  }

  getCartItems() {
    return this.cartItems;
  }

  updateOrderData(sifraPorudzbenice, sifraProizvoda, data) {
    this.ordersHistory.forEach(order => {
      if (order.sifra == sifraPorudzbenice) {
        let proizvodi = order.listaProizvoda;

        proizvodi.forEach(proizvod => {
          if (proizvod.sifraProizvoda == sifraProizvoda) {
            proizvod.rated = data.rated;
            proizvod.rate = data.rate;

            if (data.comment) {
              proizvod.comment = {
                comment: data.comment,
                user: data.user,
                date: (new Date()).toISOString(),
              };
            }
          }
        });
      }
    });
  }

  cancelOrder(sifra) {
    this.ordersActive.forEach((order, index) => {
      if (order.sifra == sifra) {
        order.status = "Otkazana";
        this.ordersHistory.push(order);
        this.ordersActive.splice(index, 1);
      }
    });
  }

  addToCart(newItem) {
    let added = false;

    this.cartItems.forEach(item => {
      if (item.sifraProizvoda == newItem.sifraProizvoda) {
        item.quantity++;
        added = true;

        return;
      }
    });

    if (!added) {
      this.cartItems.push(newItem);
    }

    this.total = 15;
  }

  deleteItem(sifra) {
    this.cartItems = this.cartItems.filter(item => item.sifraProizvoda != sifra);
  }

  updateCartItem(sifraProizvoda, quantity) {
    this.cartItems.forEach(item => {
      if (item.sifraProizvoda == sifraProizvoda) {
        item.quantity = quantity;

        return;
      }
    });
  }
}

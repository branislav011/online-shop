import { TestBed } from '@angular/core/testing';

import { DatasimService } from './datasim.service';

describe('DatasimService', () => {
  let service: DatasimService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatasimService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

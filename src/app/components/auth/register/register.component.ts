import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/auth/register/register.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { LoginService } from 'src/app/services/auth/login/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username: string;
  password: string;
  repeatPassword: string;
  firstname: string;
  lastname: string
  email: string;
  phone: string;
  address: string;
  products: string;
  result: boolean;
  productList:any = [
    "Audio komponente",
    "Zvucnici",
    "Racunari",
    "Sportska obuca",
    "Sportska Odeca",
    "Sportski Rekviziti",
    "Strucna literatura",
    "Ostala literatura",
    "Casopisi",
  ];
  
  constructor(private registerService: RegisterService, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {

  }

  register() {
    let userData = {
      username: this.username, 
      password: this.password,
      repeatPassword: this.repeatPassword,
      firstname: this.firstname,
      lastname: this.lastname,
      email: this.email,
      phone: this.phone,
      address: this.address,
      products: this.products,
      userId: this.loginService.getLastUserId(),
    };

    this.result = this.registerService.register(userData);

    if (this.result) {
      Swal.fire({
        title: 'Registracija',
        text: 'Uspesno ste napravili nalog, sada se mozete prijaviti.',
        icon: 'success',
        confirmButtonText: 'OK'
      }).then(res => {
        this.router.navigate(["login"]);
      });
    } else {
      Swal.fire({
        title: 'Registracija',
        text: 'Korisnicko ime je zauzeto, proverite jos jednom vase podatke.',
        icon: 'error',
        confirmButtonText: 'OK'
      });
    }
  }

  login() {
    this.router.navigate(["login"]);
  }

}

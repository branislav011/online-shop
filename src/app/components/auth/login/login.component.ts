import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/auth/login/login.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  result: boolean;
  
  constructor(private loginService: LoginService, private router: Router) {

  }

  ngOnInit(): void {

  }

  login() {
    this.result = this.loginService.login(this.username, this.password);

    if (this.result) {
      this.router.navigate(["home"]);
    } else {
      Swal.fire({
        title: 'Prijava',
        text: 'Uneli ste neispravne podatke, pokusajte ponovo.',
        icon: 'error',
        confirmButtonText: 'OK'
      });
    }
  }

  register() {
    this.router.navigate(["register"]);
  }

}

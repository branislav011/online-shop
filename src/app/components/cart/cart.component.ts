import { Component, OnInit } from '@angular/core';
import { DatasimService } from 'src/app/services/datasim/datasim.service';
import Swal from 'sweetalert2';
import { LoginService } from 'src/app/services/auth/login/login.service';
import { MatDialog } from '@angular/material/dialog';
import { DetailsComponent } from 'src/app/modals/details/details.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: any = [];
  showFinalOrder: boolean = false;
  termsAgree: boolean = false;
  firstname: string;
  lastname: string;
  phone: string;
  city: string;
  postalCode: string;
  address: string;
  total: string;

  constructor(private datasim: DatasimService, private loginService: LoginService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.cartItems = this.datasim.getCartItems();

    let user = this.loginService.getUserData();
    
    this.firstname = (typeof user == "undefined") ? "" : user.username;
    this.lastname = (typeof user == "undefined") ? "" : user.lastname;
    this.phone = (typeof user == "undefined") ? "" : user.phone;
    this.address = (typeof user == "undefined") ? "" : user.address;

    this.total = this.getTotal().toFixed(2);
  }

  deleteItem(sifraProizvoda) {
    this.datasim.deleteItem(sifraProizvoda);

    this.cartItems = this.datasim.getCartItems();

    this.total = this.getTotal().toFixed(2);
  }

  orderDetails() {
    this.showFinalOrder = true;
  }

  cancelOrderDetails() {
    this.showFinalOrder = false;
  }

  makeOrder() {
    Swal.fire({
      title: 'Porudzbina',
      text: 'Uspesno ste narucili proizvode. Aktuelne porudzbine mozete pogledati u delu \"Porudzbine\"',
      icon: 'success',
      confirmButtonText: 'OK'
    });

    //dodaj u porudzbenice
    let productList = [];
    
    this.cartItems.forEach(item => {
      let product = {
        "sifraProizvoda": item.sifraProizvoda,
        "vendor" : item.vendor,
        "product" : item.product,
        "rated" : false,
        "rate" : null,
        comment: null
      };

      productList.push(product);
    });

    let lastId = this.getLastId();
    let date = new Date();
    let month = date.getMonth() + 1;
    let today = date.getDate() + "." + (month > 9 ? month : "0" + month) + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();

    let data = {
      "sifra": lastId, 
      "datum" : today, 
      "status" : "Tekuca", 
      "proizvodi" : "description",
      "listaProizvoda" : productList,
      "userId" : this.loginService.getUserData().userId,
    };

    this.datasim.ordersActive.push(data);

    //isprazni korpu
    this.datasim.cartItems = [];
    this.cartItems = this.datasim.getCartItems();
  }

  getLastId() {
    let products = this.datasim.getOrderDataActive();
    let lastID = null;
    let productsCount = products.length;

    products.forEach((item, index) => {
      if ((index + 1) == productsCount) {
        lastID = item.sifra;
      }
    });

    return ++lastID;
  }

  detailsModal() {
    this.dialog.open(DetailsComponent);
  }

  getTotal() {
    let total = 0;

    this.cartItems.forEach(item => {
      let quantity = item.quantity;
      let price = item.price;
      let sum = quantity * price;

      total += sum;
    });

    return total;
  }

  updateCart(sifraProizvoda, quantity) {
    this.total = this.getTotal().toFixed(2);

    this.datasim.updateCartItem(sifraProizvoda, quantity);

    this.cartItems = this.datasim.getCartItems();
  }
}

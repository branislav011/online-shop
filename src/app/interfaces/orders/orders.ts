export interface Orders {
    userId: number;
    sifra: number;
    datum: string;
    status: string;
    proizvodi: string;
    listaProizvoda: any;
    akcije?: any;
    vendorImg?: string;
}
